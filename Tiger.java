public class Tiger{
	private String prey;
	private int speed;
	private String country;
	
	
	
	public Tiger(String prey, int speed, String country){
		this.prey = prey;
		this.speed = speed;
		this.country = country;
	}

	
	public void saySpeed(){
		if(this.speed <= 65 && this.speed > 49){
			System.out.println("Wow! Your tiger is very fast at: " + this.speed );
		}
		if(this.speed <= 49 && this.speed > 29 ){
			System.out.println("Your tiger runs at the average speed that tigers run at: " + this.speed );
		}
		
		else if(this.speed < 29){
			System.out.println("Your tiger runs pretty slowly for a tiger at: " + this.speed);
		}
	}
	public void huntPrey(){
		if(this.speed < 49){
			System.out.println("Your tiger is not trying too hard to hunt " + this.prey + " tell it to try harder");
		}
		else{
			System.out.println("Wow your tiger is running very fast to hunt " + this.prey + " you should go congratulate it");
		}
	}
	
	//getter methods
	
	public String getPrey(){
		return this.prey;
	}
	
	public int getSpeed(){
		return this.speed;
	}
	
	public String getCountry(){
		return this.country;
	}
	
	//setter methods
	
	public void setCountry(String newCountry){
		this.country = newCountry;
	}
}